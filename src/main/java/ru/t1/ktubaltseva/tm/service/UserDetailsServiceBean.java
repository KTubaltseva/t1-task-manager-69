package ru.t1.ktubaltseva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.IUserService;
import ru.t1.ktubaltseva.tm.dto.model.CustomUser;
import ru.t1.ktubaltseva.tm.enumerated.RoleType;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.model.Role;
import ru.t1.ktubaltseva.tm.model.User;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @NotNull
    @Autowired
    private IUserService userService;

    @SneakyThrows
    @NotNull
    @Override
    public UserDetails loadUserByUsername(
            @NotNull final String username
    ) throws UsernameNotFoundException {
        @Nullable final User user = userService.findByLogin(username);
        @NotNull final List<Role> userRoles = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        for (@NotNull final Role role : userRoles) {
            roles.add(role.toString());
        }
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    private void init() throws AbstractException {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("user", "user", RoleType.USUAL);
    }

    private void initUser(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final RoleType roleType
    ) throws AbstractException {
        @Nullable final User user = userService.findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Transactional
    public void createUser(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final RoleType roleType
    ) throws EntityNotFoundException {
        if (login.isEmpty()) return;
        if (password.isEmpty()) return;
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userService.add(user);
    }

}
