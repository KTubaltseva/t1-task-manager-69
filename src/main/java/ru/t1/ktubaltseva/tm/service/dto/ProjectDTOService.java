package ru.t1.ktubaltseva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.repository.dto.ProjectDTORepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTOService extends AbstractDTOService<ProjectDTO, ProjectDTORepository> implements IProjectDTOService {

    @NotNull
    @Autowired
    private ProjectDTORepository repository;

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public ProjectDTO findById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<ProjectDTO> optionalM = repository.findByUserIdAndId(userId, id);
        return optionalM.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public long count(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return repository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void delete(@Nullable final String userId, @Nullable final ProjectDTO model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO update(
            @Nullable final String userId,
            @Nullable final ProjectDTO model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        if (!repository.existsByUserIdAndId(userId, model.getId())) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

    @Override
    public @NotNull ProjectDTO create(@Nullable final String userId) throws EntityNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public @NotNull ProjectDTO create() throws EntityNotFoundException {
        return add(new ProjectDTO());
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO add(@Nullable final String userId, @Nullable final ProjectDTO model) throws EntityNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        return repository.saveAndFlush(model);
    }

}
