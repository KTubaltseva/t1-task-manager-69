package ru.t1.ktubaltseva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.model.ProjectRepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectService extends AbstractService<Project, ProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    @Transactional
    public void clear(@Nullable final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        repository.deleteAllByUser(user);
    }

    @Override
    public boolean existsById(@Nullable final User user, @Nullable final String id) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserAndId(user, id);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        return repository.findAllByUser(user);
    }

    @NotNull
    @Override
    public Project findById(@Nullable final User user, @Nullable final String id) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<Project> optionalM = repository.findByUserAndId(user, id);
        return optionalM.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public long count(@Nullable final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        return repository.countByUser(user);
    }

    @Override
    @Transactional
    public void delete(@Nullable final User user, @Nullable final Project model) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        repository.deleteByUserAndId(user, model.getId());
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final User user, @Nullable final String id) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        repository.deleteByUserAndId(user, id);
    }

    @NotNull
    @Override
    @Transactional
    public Project update(
            @Nullable final User user,
            @Nullable final Project model
    ) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        if (!repository.existsByUserAndId(user, model.getId())) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

    @Override
    @Transactional
    public @NotNull Project create() throws EntityNotFoundException {
        return add(new Project());
    }
}
