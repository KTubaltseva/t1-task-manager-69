package ru.t1.ktubaltseva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.PasswordEmptyException;
import ru.t1.ktubaltseva.tm.repository.dto.UserDTORepository;

import java.security.NoSuchAlgorithmException;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserDTOService extends AbstractDTOService<UserDTO, UserDTORepository> implements IUserDTOService {

    @NotNull
    @Autowired
    private UserDTORepository repository;

    public @NotNull UserDTO create() throws EntityNotFoundException {
        return add(new UserDTO());
    }

    @NotNull
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @NotNull
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.existsByLogin(login);
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.existsByEmail(email);
    }

    @NotNull
    @Override
    public UserDTO lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = findByLogin(login);
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        repository.deleteByLogin(login);
    }

    @Override
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        repository.deleteByEmail(email);
    }

    @NotNull
    @Override
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable UserDTO resultUser = findById(id);
        return resultUser;
//        resultUser.setPasswordHash(HashUtil.salt(propertyService, password));
//        return update(resultUser);
    }

    @NotNull
    @Override
    public UserDTO unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = findByLogin(login);
        resultUser.setLocked(false);
        return update(resultUser);
    }

}
