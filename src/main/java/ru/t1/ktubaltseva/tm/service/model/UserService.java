package ru.t1.ktubaltseva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.IUserService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.PasswordEmptyException;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.model.UserRepository;

import java.security.NoSuchAlgorithmException;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserService extends AbstractService<User, UserRepository> implements IUserService {

    @NotNull
    @Autowired
    private UserRepository repository;

    @Transactional
    public @NotNull User create() throws EntityNotFoundException {
        return add(new User());
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.existsByLogin(login);
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.existsByEmail(email);
    }

    @NotNull
    @Override
    @Transactional
    public User lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        repository.deleteByLogin(login);
    }

    @Override
    @Transactional
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        repository.deleteByEmail(email);
    }

    @NotNull
    @Override
    @Transactional
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable User resultUser = findById(id);
        return resultUser;
//        resultUser.setPasswordHash(HashUtil.salt(propertyService, password));
//        return update(resultUser);
    }

    @NotNull
    @Override
    @Transactional
    public User unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(false);
        return update(resultUser);
    }

}
