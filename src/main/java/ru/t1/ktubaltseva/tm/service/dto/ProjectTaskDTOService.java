package ru.t1.ktubaltseva.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.repository.dto.ProjectDTORepository;
import ru.t1.ktubaltseva.tm.repository.dto.TaskDTORepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskDTOService implements IProjectTaskDTOService {

    @Getter
    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @Override
    @Transactional
    public void deleteByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final Optional<ProjectDTO> project = getProjectRepository().findByUserIdAndId(userId, projectId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        getTaskRepository().deleteAllByUserIdAndProjectId(userId, projectId);
        getProjectRepository().deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    public void deleteByUserId(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final List<ProjectDTO> projects = getProjectRepository().findAllByUserId(userId);
        for (@NotNull final ProjectDTO project : projects) {
            getTaskRepository().deleteAllByUserIdAndProjectId(userId, project.getId());
        }
        getProjectRepository().deleteAllByUserId(userId);
    }

}

