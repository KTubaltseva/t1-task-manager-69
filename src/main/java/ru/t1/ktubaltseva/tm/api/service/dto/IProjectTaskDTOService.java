package ru.t1.ktubaltseva.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public interface IProjectTaskDTOService {

    void deleteByUserIdAndProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException;

    void deleteByUserId(@Nullable String userId) throws AbstractException;

}
