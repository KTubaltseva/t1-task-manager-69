package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends IDatabaseProperty {

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getSessionKey();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHBM2DDL();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseSecondLvlCache();

    @NotNull
    String getDatabaseFactoryClass();

    @NotNull
    String getDatabaseUseQueryCache();

    @NotNull
    String getDatabaseUseMinPuts();

    @NotNull
    String getDatabaseRegionPrefix();

    @NotNull
    String getDatabaseConfigFilePath();

    @NotNull
    String getDatabaseInitToken();
}
