package ru.t1.ktubaltseva.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;

public interface IProjectTaskService {

    void deleteByUserAndProject(
            @Nullable User user,
            @Nullable Project project
    ) throws AbstractException;

    void deleteByUser(@Nullable User user) throws AbstractException;

}
