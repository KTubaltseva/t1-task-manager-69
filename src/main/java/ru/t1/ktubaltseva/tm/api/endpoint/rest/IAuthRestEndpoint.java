package ru.t1.ktubaltseva.tm.api.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@RestController
@RequestMapping("/api/auth")
public interface IAuthRestEndpoint {

    @PostMapping(value = "/login")
    boolean login(
            @RequestParam("username") @NotNull final String username,
            @RequestParam("password") @NotNull final String password
    );

    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    boolean logout();

    @NotNull
    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    UserDTO profile() throws AbstractException;

}

