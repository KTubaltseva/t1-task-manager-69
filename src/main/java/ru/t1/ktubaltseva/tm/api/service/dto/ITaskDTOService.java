package ru.t1.ktubaltseva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;

import java.util.List;

public interface ITaskDTOService extends IDTOService<TaskDTO> {

    @NotNull
    TaskDTO create(@Nullable String userId) throws EntityNotFoundException, UserNotFoundException;

    @NotNull
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO model) throws EntityNotFoundException, UserNotFoundException;

    void clear(@Nullable String userId) throws AbstractException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId) throws AbstractException;

    @NotNull
    TaskDTO findById(@Nullable String userId, @Nullable String id) throws AbstractException;

    long count(@Nullable String userId) throws AbstractException;

    void delete(@Nullable String userId, @Nullable TaskDTO model) throws AbstractException;

    void deleteById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    TaskDTO update(@Nullable String userId, @Nullable TaskDTO model) throws AbstractException;

    @NotNull
    List<TaskDTO> findAllByUserAndProject(@Nullable final String userId, @Nullable final String projectId) throws AbstractException;

}
