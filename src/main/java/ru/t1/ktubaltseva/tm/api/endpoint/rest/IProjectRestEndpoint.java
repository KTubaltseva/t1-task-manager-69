package ru.t1.ktubaltseva.tm.api.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/projects")
public interface IProjectRestEndpoint {

    @NotNull
    @PutMapping(value = "/create", produces = APPLICATION_JSON_VALUE)
    ProjectDTO create() throws AbstractException;

    @NotNull
    @PutMapping(value = "/add", produces = APPLICATION_JSON_VALUE)
    ProjectDTO add(@RequestBody @NotNull ProjectDTO model) throws AbstractException;

    @DeleteMapping(value = "/delete/{id}")
    void deleteById(@PathVariable("id") @NotNull String id) throws AbstractException;

    @GetMapping(value = "/exists/{id}", produces = APPLICATION_JSON_VALUE)
    boolean existsById(@PathVariable("id") @NotNull String id) throws AbstractException;

    @NotNull
    @GetMapping(value = "/find/{id}", produces = APPLICATION_JSON_VALUE)
    ProjectDTO findById(@PathVariable("id") @NotNull String id) throws AbstractException;

    @NotNull
    @PutMapping(value = "/update", produces = APPLICATION_JSON_VALUE)
    ProjectDTO update(@RequestBody @NotNull ProjectDTO model) throws AbstractException;

    @NotNull
    @GetMapping(value = "/findAll", produces = APPLICATION_JSON_VALUE)
    List<ProjectDTO> findAll() throws AbstractException;

    @DeleteMapping(value = "/clear", produces = APPLICATION_JSON_VALUE)
    void clear() throws AbstractException;

    @GetMapping(value = "/count", produces = APPLICATION_JSON_VALUE)
    long count() throws AbstractException;

}