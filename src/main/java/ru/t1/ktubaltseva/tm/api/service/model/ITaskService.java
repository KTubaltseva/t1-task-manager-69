package ru.t1.ktubaltseva.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void clear(@Nullable User user) throws AbstractException;

    boolean existsById(@Nullable User user, @Nullable String id) throws AbstractException;

    @NotNull
    List<Task> findAll(@Nullable User user) throws AbstractException;

    @NotNull
    Task findById(@Nullable User user, @Nullable String id) throws AbstractException;

    long count(@Nullable User user) throws AbstractException;

    void delete(@Nullable User user, @Nullable Task model) throws AbstractException;

    void deleteById(@Nullable User user, @Nullable String id) throws AbstractException;

    @NotNull
    Task update(@Nullable User user, @Nullable Task model) throws AbstractException;

    @NotNull
    List<Task> findAllByUserAndProject(@Nullable User user, @Nullable Project project) throws AbstractException;

}
