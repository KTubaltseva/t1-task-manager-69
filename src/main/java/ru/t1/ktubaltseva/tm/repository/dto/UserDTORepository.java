package ru.t1.ktubaltseva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;

@Repository
@Scope("prototype")
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    UserDTO findByLogin(@NotNull String login);

    UserDTO findByEmail(@NotNull String Email);

    void deleteByLogin(@NotNull String login);

    void deleteByEmail(@NotNull String email);

    Boolean existsByLogin(@NotNull String login);

    Boolean existsByEmail(@NotNull String email);

}
