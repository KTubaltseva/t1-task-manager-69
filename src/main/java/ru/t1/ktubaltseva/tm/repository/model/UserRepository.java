package ru.t1.ktubaltseva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.model.User;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    User findByLogin(@NotNull String login);

    User findByEmail(@NotNull String Email);

    void deleteByLogin(@NotNull String login);

    void deleteByEmail(@NotNull String email);

    Boolean existsByLogin(@NotNull String login);

    Boolean existsByEmail(@NotNull String email);

}
