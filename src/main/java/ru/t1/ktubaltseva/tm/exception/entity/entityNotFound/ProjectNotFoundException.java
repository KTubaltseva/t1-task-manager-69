package ru.t1.ktubaltseva.tm.exception.entity.entityNotFound;

public final class ProjectNotFoundException extends AbstractEntityNotFoundException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
