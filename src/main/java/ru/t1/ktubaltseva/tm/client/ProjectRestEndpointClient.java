package ru.t1.ktubaltseva.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.List;

public interface ProjectRestEndpointClient {

    static ProjectRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, "http://localhost:8080/api/projects");
    }

    @PutMapping("/create")
    ProjectDTO create() throws AbstractException;

    @PutMapping("/add")
    ProjectDTO add(@RequestBody @NotNull final ProjectDTO project) throws AbstractException;

    @DeleteMapping("/delete/{id}")
    void deleteById(@PathVariable("id") @NotNull final String id) throws AbstractException;

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") @NotNull final String id) throws AbstractException;

    @GetMapping("/findById/{id}")
    ProjectDTO findById(@PathVariable("id") @NotNull final String id) throws AbstractException;

    @PostMapping("/update/{id}")
    ProjectDTO update(@RequestBody @NotNull final ProjectDTO project) throws AbstractException;

    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @DeleteMapping("/clear")
    void clear();

    @GetMapping("/count")
    long count();

}
