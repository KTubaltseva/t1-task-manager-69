package ru.t1.ktubaltseva.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public interface UserRestEndpointClient {

    static UserRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(UserRestEndpointClient.class, "http://localhost:8080/api/users");
    }
    
    @GetMapping(value = "/count", produces = APPLICATION_JSON_VALUE)
    long count() throws AbstractException;

    @DeleteMapping(value = "/delete", produces = APPLICATION_JSON_VALUE)
    void clear() throws AbstractException;

    @DeleteMapping(value = "/delete/{id}", produces = APPLICATION_JSON_VALUE)
    void deleteById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @GetMapping(value = "/exists/{id}", produces = APPLICATION_JSON_VALUE)
    boolean existsById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @GetMapping(value = "/findAll", produces = APPLICATION_JSON_VALUE)
    List<UserDTO> findAll() throws AbstractException;

    @NotNull
    @GetMapping(value = "/findById/{id}", produces = APPLICATION_JSON_VALUE)
    UserDTO findById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException;

    @NotNull
    @PutMapping(value = "/add", produces = APPLICATION_JSON_VALUE)
    UserDTO save(
            @RequestBody @NotNull final UserDTO user
    ) throws EntityNotFoundException;

    @NotNull
    @PostMapping(value = "/save/{id}", produces = APPLICATION_JSON_VALUE)
    UserDTO update(
            @RequestBody @NotNull final UserDTO user
    ) throws AbstractException;

}
