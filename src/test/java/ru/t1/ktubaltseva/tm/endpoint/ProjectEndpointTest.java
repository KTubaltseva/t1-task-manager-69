package ru.t1.ktubaltseva.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.client.ProjectRestEndpointClient;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.marker.IntegrationCategory;

import java.util.Collection;

import static ru.t1.ktubaltseva.tm.constant.ProjectTestData.*;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest extends AbstractTest {

    @NotNull
    private final ProjectRestEndpointClient projectEndpointClient = ProjectRestEndpointClient.client();

    @BeforeClass
    @SneakyThrows
    public static void beforeClazz() {
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
    }

    @After
    @SneakyThrows
    public void after() {
        if (projectEndpointClient.existsById(MODEL_1.getId()))
            projectEndpointClient.deleteById(MODEL_1.getId());
    }

    @Test
    public void create() throws AbstractException {
        @Nullable final ProjectDTO projectToAdd = MODEL_1;
        @Nullable final String projectToAddId = projectToAdd.getId();

        @Nullable final ProjectDTO projectAdded = projectEndpointClient.add(projectToAdd);
        Assert.assertNotNull(projectAdded);
        Assert.assertEquals(projectToAdd.getId(), projectAdded.getId());

        @Nullable final ProjectDTO projectFindOneById = projectEndpointClient.findById(projectToAddId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectToAdd.getId(), projectFindOneById.getId());
    }

    @Test
    public void findById() throws AbstractException {
        @NotNull final ProjectDTO projectExists = MODEL_1;
        projectEndpointClient.add(projectExists);

        @Nullable final ProjectDTO projectFindOneById = projectEndpointClient.findById(projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());
    }


    @Test
    public void findAll() throws AbstractException {
        @NotNull final ProjectDTO projectExists = MODEL_1;
        projectEndpointClient.add(projectExists);

        @NotNull final Collection<ProjectDTO> projectsFindAllNoEmpty = projectEndpointClient.findAll();
        Assert.assertNotNull(projectsFindAllNoEmpty);
    }

    @Test
    @Ignore
    public void clear() {
        projectEndpointClient.clear();
        Assert.assertEquals(0, projectEndpointClient.count());
    }

    @Test
    public void deleteById() throws AbstractException {
        @Nullable final ProjectDTO projectToRemove = MODEL_1;
        projectEndpointClient.add((projectToRemove));

        projectEndpointClient.deleteById(projectToRemove.getId());

        Assert.assertFalse(projectEndpointClient.existsById(projectToRemove.getId()));
    }

    @Test
    public void existsById() throws AbstractException {
        @NotNull final ProjectDTO projectExists = MODEL_1;
        projectEndpointClient.add(projectExists);

        Assert.assertFalse(projectEndpointClient.existsById(NON_EXISTENT_MODEL_ID));
        Assert.assertTrue(projectEndpointClient.existsById(projectExists.getId()));
    }

    @Test
    public void updateProjectById() throws AbstractException {
        @Nullable final ProjectDTO projectToUpdate = projectEndpointClient.add((MODEL_1));
        projectToUpdate.setName(MODEL_NAME);

        @Nullable final ProjectDTO projectUpdated = projectEndpointClient.update(projectToUpdate);
        Assert.assertNotNull(projectUpdated);
        Assert.assertEquals(projectUpdated.getId(), projectToUpdate.getId());
        Assert.assertEquals(projectToUpdate.getName(), projectUpdated.getName());
    }

}
